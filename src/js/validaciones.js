'use strict'


	/* ------------ Variables ------------ */
	const INPUT_NOMBRE = document.getElementById('nombre_paciente'),
		  INPUT_APELLIDO = document.getElementById('apellido_paciente'),
		  INPUT_TELEFONO = document.getElementById('telefono_paciente'),
		  INPUT_EMAIL = document.getElementById('email_paciente'),
		  INPUT_AUTOCOMPLETE = document.getElementById('autocomplete'),
		  INPUT_PAIS = document.getElementById('country'),
		  INPUT_ESTADO = document.getElementById('administrative_area_level_1'),
		  INPUT_CIUDAD = document.getElementById('locality'),
		  INPUT_DIRECCION = document.getElementById('street_number'),
		  INPUT_CODPOSTAL = document.getElementById('postal_code'),
		  INPUT_PASSWORD = document.getElementById('password_paciente'),
		  INPUT_REPEAT_PASSWORD = document.getElementById('repeatPassword'),
		  TERMINOS_Y_CONDICIONES = document.getElementById('terminosycondiciones'),
		  NOTIFICACIONES_KAMY = document.getElementById('notificaciones'),
		  TOKEN_CAPTCHA = document.getElementById('tokenCaptcha'),
		  FORMULARIO = document.getElementById('formulario'),
		  SEND_BTN = document.getElementById('send_btn'),

		  // Help
		  AYUDA_NOMBRE = document.getElementById('name_help'),
		  AYUDA_APELLIDO = document.getElementById('lastName_help'),
		  AYUDA_TELEFONO = document.getElementById('phone_help'),
		  AYUDA_EMAIL = document.getElementById('email_help'),
		  AYUDA_PASSWORD = document.getElementById('ayuda_password'),
		  AYUDA_REPEAT_PASSWORD = document.getElementById('ayuda_repeat_password'),

		  // Regular Expressions
		  REGEXP_TEXTO = /^[ a-zA-Z\u00E0-\u00FC]+$/,
		  REGEXP_CODPOSTAL = /[\d]{4,5}/,
		  REGEXP_TELEFONO = /^[\+\(]?[\d]+?[\)]?[\d \-\.]+$/,
		  REGEXP_EMAIL = /[\w\.]+@[\w]+[\.][\w]+/,
		  REGEXP_DIRECCION = /[\w\.\, \(\)\-\u00E0-\u00FC]+/,
		  REGEXP_ESTADO = /^[ a-zA-Z\u00E0-\u00FC\.]+$/,
		  REGEXP_MINUSCULA = /[a-z]/,
		  REGEXP_MAYUSCULA = /[A-Z]/,
		  REGEXP_NUMERO = /[0-9]/,
		  REGEXP_ESPECIALES = /[\.\@\?\!\$\%\#\*\[\]\{\}]/;


	/* ------------ Functions ------------ */
	/**
	 * @function VALIDAR_CAMPO
	 * @param {RegExp} regExp - Expresion regular que validara
	 * @param {String|Number|Email|Phone} valor - Contenido a validar
	 * @description Valida el parametro "valor" a traves de una expresion regular y otros tipos de validacion.
	 */
	const VALIDAR_CAMPO = (regExp, valor, minLength, maxLength) => {

		return (regExp.test(valor) && valor !== "" && valor !== null && valor !== undefined 
			   && valor.length >= minLength && valor.length <= maxLength)?true:false;

	}

	/**
	 * @function VALIDAR_PASSWORD
	 * @param {String} clave - Valor del campo password
	 * @param {String} claveRepetida - Valor del campo repeat-password
	 * @description Valida el parametro "valor" a traves de una expresion regular y otros tipos de validacion.
	 */
	const VALIDAR_PASSWORD = (clave="", claveRepetida="") => {

		// Validar expresiones regulares
		if(REGEXP_MINUSCULA.test(clave) && REGEXP_MAYUSCULA.test(clave) 
		 && REGEXP_NUMERO.test(clave) && REGEXP_ESPECIALES.test(clave)){
			
			// Validar longitud de la clave
			if( clave.length >= 8 && clave.length <= 60 ){

				// Validar igualacion de clave y clave repetida
				if( clave === claveRepetida ){

					AYUDA_PASSWORD.innerHTML = `✓ Perfecto`;

					INPUT_PASSWORD.classList.add("validado");
					INPUT_PASSWORD.classList.remove("error");

					INPUT_REPEAT_PASSWORD.classList.add("validado");
					INPUT_REPEAT_PASSWORD.classList.remove("error");
					return true;
					
				}else{

					// Si la clave y clave repetida no es igual
					AYUDA_PASSWORD.innerHTML = `Ambas contraseñas deben ser iguales.`;

					INPUT_PASSWORD.classList.add("error");
					INPUT_PASSWORD.classList.remove("validado");

					INPUT_REPEAT_PASSWORD.classList.add("error");
					INPUT_REPEAT_PASSWORD.classList.remove("validado");
					return false;

				}

			}else{

				// Si la longitud no es la correcta
				INPUT_PASSWORD.classList.add("error");
				INPUT_PASSWORD.classList.remove("validado");
				AYUDA_PASSWORD.innerHTML = `Debe contener entre 8 a 60 caracteres.`;
				return false;

			}

		}else{
			// Si no se cumplen todas las expresiones regulares
			INPUT_PASSWORD.classList.add("error");
			INPUT_PASSWORD.classList.remove("validado");
			AYUDA_PASSWORD.innerHTML = `Debe contener: minúscula, mayúscula, número, carácter especial.`;
			return false;
		}

	}

	/**
	 * @function BORDES_DEL_INPUT
	 * @param {Object} input - Campo input
	 * @param {Boolean} boolean - Resultado que proviene de validar el campo mediante la funcion VALIDAR_CAMPO.
	 * @description Valida el parametro "valor" a traves de una expresion regular y otros tipos de validacion.
	 */
	const BORDES_DEL_INPUT = (input, boolean) => {

		if( boolean ){
			input.classList.add("validado"); // Borde del campo en verde
			input.classList.remove("error"); 
		}else{
			input.classList.add("error"); 	 // Borde del campo en rojo
			input.classList.remove("validado");
		}

	}

	/**
	 * @function COMPROBAR_CAMPOS
	 * @description Comprobar datos no rellenados.
	 */
	const COMPROBAR_CAMPOS = () => {

		let lista_input = document.getElementsByClassName('input');

		for( let a=0; a < lista_input.length; a++ ){
			if(lista_input[a].value == null || lista_input[a].value == undefined || lista_input[a].value == "" ){
				// Si hay un error
				lista_input[a].classList.add("error"); 	 // Borde del campo en rojo
				lista_input[a].classList.remove("validado");
			}else{
				// Si no hay error
				lista_input[a].classList.add("validado"); // Borde del campo en verde
				lista_input[a].classList.remove("error"); 
			}
		}

	}

	/**
	 * @function enviarDatos
	 * @param {String} token - Token de g-recaptcha
	 * @param {Boolean} terms - Comprueba si terminos y condiciones tiene o no el check.
	 * @param {Boolean} notifications - Comprueba si notificaciones de kamy tiene o no el check.
	 * @description Envia los datos al servidor.
	 */
	function enviarDatos(token,terms,notifications){

		let json = {
			"name": INPUT_NOMBRE.value,
			"lastName": INPUT_APELLIDO.value,
			"phone": INPUT_TELEFONO.value,
			"address": INPUT_DIRECCION.value,
			"email": INPUT_EMAIL.value,
			"password": INPUT_PASSWORD.value,
			"passwordConfirm": INPUT_REPEAT_PASSWORD.value,
			"city": INPUT_CIUDAD.value,
			"postalCode": INPUT_CODPOSTAL.value,
			"country": INPUT_PAIS.value,
			"state": INPUT_ESTADO.value,
			"subscription": notifications,
			"terms": terms,
  		"tokenCaptcha": `${token}`
		}

		json = JSON.stringify(json);

		var xhr = new XMLHttpRequest();

		/**
		 * @function onRequestHandler
		 * @description Comprueba la respuesta del servidor.
		 */
		function onRequestHandler(){

			if( this.readyState == 4 && this.status == 200 ){
					alert('Perfecto! tu registro ha sido exitoso.');
					location.href = "./verificar.html?name="+INPUT_NOMBRE.value+"&mail="+INPUT_EMAIL.value; // Esta ventana se muestra si la respuesta del backend es satisfactoria
			}else{
					let respuesta_json = this.response,
							respuesta = JSON.parse(respuesta_json);
					alert(respuesta.error);
			}

		}

		xhr.addEventListener('load', onRequestHandler);
		xhr.open("POST", `https://holakamy-backend.herokuapp.com/api/v1/user/patient`);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.setRequestHeader("Accept", "application/json");
		xhr.send(json);

	}
		
	/**
	 * @function COMPROBAR_DATOS
	 * @param {String} tokenCaptcha - Token enviado desde la funcion onSubmit
	 * @description Se encarga de validar todo el formulario 
	   (incluyendo el token de g-recaptcha para que este no llegue vacio)
	 */
	const COMPROBAR_DATOS = tokenCaptcha => {

		if ( VALIDAR_PASSWORD(INPUT_PASSWORD.value, INPUT_REPEAT_PASSWORD.value) ){
			INPUT_PASSWORD.classList.add("validado"); // Borde del campo en verde
			INPUT_PASSWORD.classList.remove("error"); 

			INPUT_REPEAT_PASSWORD.classList.add("validado"); // Borde del campo en verde
			INPUT_REPEAT_PASSWORD.classList.remove("error"); 
		}

		if( VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_NOMBRE.value,3,30) && VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_APELLIDO.value,3,30)
			&& VALIDAR_CAMPO(REGEXP_TELEFONO,INPUT_TELEFONO.value,10,15) && VALIDAR_CAMPO(REGEXP_EMAIL,INPUT_EMAIL.value,11,76) 
			&& VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_PAIS.value,3,15) && VALIDAR_CAMPO(REGEXP_ESTADO,INPUT_ESTADO.value,3,15) 
			&& VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_CIUDAD.value,3,30) && VALIDAR_CAMPO(REGEXP_DIRECCION,INPUT_DIRECCION.value,5,50) 
			&& VALIDAR_CAMPO(REGEXP_CODPOSTAL,INPUT_CODPOSTAL.value,4,5) && VALIDAR_PASSWORD(INPUT_PASSWORD.value, INPUT_REPEAT_PASSWORD.value) 
			&& tokenCaptcha !== null && tokenCaptcha !== undefined && tokenCaptcha !== ""){

				// Validamos la aceptacion de términos y las condiciones
				if( TERMINOS_Y_CONDICIONES.checked === true ){

					// Guardar token en input
					TOKEN_CAPTCHA.value = tokenCaptcha;

					// Comprobar si el navegador soporta localStorage
					if( typeof(Storage) !== "undefined" ){
						localStorage.setItem("email",INPUT_EMAIL.value);
						localStorage.setItem("tokenCaptcha",tokenCaptcha);
					}

					// Send data to back end
					enviarDatos(tokenCaptcha,TERMINOS_Y_CONDICIONES.checked,NOTIFICACIONES_KAMY.checked);

					// Ocultar el spinner y colocar Enviar nuevamente
					SEND_BTN.innerHTML = `ENVIAR`;

					e.preventDefault();

				}else{

					alert('Acepte los términos y condiciones para terminar el registro!');

				}

				
		}else{

				// Detectar campos vacios
				COMPROBAR_CAMPOS();

				// Si algun campo no es "true".
				alert('Corrija los campos marcados en rojo');

		}

		SEND_BTN.innerHTML = `ENVIAR`;

	}

	/**
	 * @function onSubmit
	 * @param {String} token - Token devuelto por g-recaptcha
	 * @description Se ejecuta al enviar el formulario
	 */
	function onSubmit(token) {

		// Bordes rojos para Password y Repeat-password
		INPUT_PASSWORD.classList.add("error");
		INPUT_REPEAT_PASSWORD.classList.add("error");

		// SPINNER
		SEND_BTN.innerHTML = `<div class="spinner-border text-light" role="status">
													<span class="visually-hidden">Loading...</span>
												  </div>`;


		COMPROBAR_DATOS(token);
		
	}

	/**
	 * @function helps
	 * @param {String} campo - Campo donde que mostrara el texto de ayuda
	 * @param {String} boolean - Comprobacion de campo para verificar que cumple con la expresion regular
	 * @param {String} text - Texto que se mostrara
	 * @description Se ejecuta al enviar el formulario
	 */
	function helps(campo,boolean,text){

		if( boolean == false ){
			campo.innerHTML = text;
		}else{
			campo.innerHTML = `✓ Perfecto`;
		}

	}


	/* ------------ Eventos ----------- */
	INPUT_NOMBRE.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_NOMBRE.value,3,30); // True|False
		if( INPUT_NOMBRE.value.length < 3 ){

			helps(AYUDA_NOMBRE,boolean,'*Necesita al menos 3 caracteres*');

		}else if( INPUT_NOMBRE.value.length > 30  ){

			helps(AYUDA_NOMBRE,boolean,'*Excediste el número máximo de caracteres*');

		}else{

			helps(AYUDA_NOMBRE,boolean,'*Solo se permite letras y espacios*');

		}
		
		BORDES_DEL_INPUT(INPUT_NOMBRE, boolean);
	});

	INPUT_APELLIDO.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_APELLIDO.value,3,30);

		if( INPUT_APELLIDO.value.length < 3 ){

			helps(AYUDA_APELLIDO,boolean,'*Necesita al menos 3 caracteres*');

		}else if( INPUT_APELLIDO.value.length > 30  ){

			helps(AYUDA_APELLIDO,boolean,'*Excediste el número máximo de caracteres*');

		}else{

			helps(AYUDA_APELLIDO,boolean,'*Solo se permite letras y espacios*');

		}

		BORDES_DEL_INPUT(INPUT_APELLIDO, boolean);
	});

	INPUT_TELEFONO.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_TELEFONO,INPUT_TELEFONO.value,10,15);

		if( INPUT_TELEFONO.value.length < 10 ){

			helps(AYUDA_TELEFONO,boolean,'*Necesita al menos 10 caracteres*');

		}else if( INPUT_TELEFONO.value.length > 15  ){

			helps(AYUDA_TELEFONO,boolean,'*Excediste el número máximo de caracteres*');

		}else{

			helps(AYUDA_TELEFONO,boolean,'*Solo se permite números, espacios y caracteres como: +()-. *');

		}

		BORDES_DEL_INPUT(INPUT_TELEFONO, boolean);
	});

	INPUT_EMAIL.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_EMAIL,INPUT_EMAIL.value,11,76);

		if( INPUT_EMAIL.value.length < 11 ){

			helps(AYUDA_EMAIL,boolean,'*Necesita al menos 11 caracteres*');

		}else if( INPUT_EMAIL.value.length > 76  ){

			helps(AYUDA_EMAIL,boolean,'*Excediste el número máximo de caracteres*');

		}else{

			helps(AYUDA_EMAIL,boolean,'*Solo se permite emails de tipo: ejemplo@ejemplo.com*');

		}

		BORDES_DEL_INPUT(INPUT_EMAIL, boolean);
	});


	// Direccion
	INPUT_PAIS.addEventListener('change', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_PAIS.value,3,15);
		BORDES_DEL_INPUT(INPUT_PAIS, boolean);
	});

	INPUT_ESTADO.addEventListener('change', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_ESTADO,INPUT_ESTADO.value,3,15);
		BORDES_DEL_INPUT(INPUT_ESTADO,boolean)
	});

	INPUT_CIUDAD.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_TEXTO,INPUT_CIUDAD.value,3,30);
		BORDES_DEL_INPUT(INPUT_CIUDAD, boolean);
	});

	INPUT_DIRECCION.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_DIRECCION,INPUT_DIRECCION.value,5,50);
		BORDES_DEL_INPUT(INPUT_DIRECCION, boolean);
	});

	INPUT_CODPOSTAL.addEventListener('keyup', () => {
		let boolean = VALIDAR_CAMPO(REGEXP_CODPOSTAL,INPUT_CODPOSTAL.value,4,5);
		BORDES_DEL_INPUT(INPUT_CODPOSTAL, boolean);
	});


	// Seguridad
	INPUT_PASSWORD.addEventListener('keyup', () => {
		VALIDAR_PASSWORD(INPUT_PASSWORD.value, INPUT_REPEAT_PASSWORD.value);
	});

	INPUT_REPEAT_PASSWORD.addEventListener('keyup', () => {
		VALIDAR_PASSWORD(INPUT_PASSWORD.value, INPUT_REPEAT_PASSWORD.value);
	});
	

	// Autocomplete
	INPUT_AUTOCOMPLETE.addEventListener('focus', () => {
		INPUT_AUTOCOMPLETE.parentNode.classList.remove('col-12');
		INPUT_AUTOCOMPLETE.parentNode.classList.add('col-10', 'col-md-11');
		$('#spinner').show();
	});

	INPUT_AUTOCOMPLETE.addEventListener('blur', () => {
		INPUT_AUTOCOMPLETE.parentNode.classList.remove('col-10', 'col-md-11');
		INPUT_AUTOCOMPLETE.parentNode.classList.add('col-12');
		$('#spinner').hide();
	});

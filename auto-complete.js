
window.addEventListener('DOMContentLoaded', () => {

    // Activate autocomplete input
    let autocomplete_input = document.getElementById('autocomplete'); 
        autocomplete_input.removeAttribute("disabled");

    $('#autocomplete').keyup(function(){
      $('#spinner').show();
    });

});

// Funcion de Google Maps API
google.maps.event.addDomListener(window, 'load', initAutocomplete);


/* ------------ Variables ----------- */
var placeSearch, autocomplete;
var componentForm = {
  country: 'long_name',
  locality: 'long_name',
  street_number: 'short_name',
  postal_code: 'short_name'
};


/**
  * @function initAutocomplete
  * @description Permite hacer el autocompletado del campo de direccion (id="autocomplete")
 */
function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
    {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}


function validarCampos(input){

  // activar Pais
  if( input.value !== null && input.value !== undefined && input.value !== "" ){
    input.classList.add("validado"); // Borde del campo en verde
    input.classList.remove("error");
  }else{
    input.classList.add("error");
    input.classList.remove("validado");
  }

}


/**
  * @function fillInAddress
  * @description Se ejecuta al colocar un nueva direccion en el campo autocomplete (id="autocomplete")
 */
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Activate municipality input
  let municipality_input = document.getElementById('municipality'); 
      municipality_input.removeAttribute("disabled");

  // Hide autocomplete spinner
  $('#spinner').hide();

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }


  // activar campos
  validarCampos(INPUT_AUTOCOMPLETE);
  validarCampos(INPUT_PAIS);
  validarCampos(INPUT_CIUDAD);
  validarCampos(INPUT_MUNICIPIO);
  validarCampos(INPUT_DIRECCION);
  validarCampos(INPUT_CODPOSTAL);

}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
/**
  * @function geolocate
  * @description Se ejecuta cuando el campo autocomplete (id="autocomplete") tiene el foco.
 */
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
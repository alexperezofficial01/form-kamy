'use strict'


/* ------------ Variables ----------- */
const CAMPOS = document.getElementsByClassName('verification_input'),
	  	CANTIDAD_DE_CAMPOS = CAMPOS.length,
	  	REGEXP = /[\w]{1}/;


/* ------------ Funciones ----------- */

	/**
   * @function getQueryVariable
	 * @param {String} variable
	 * @return String
	 * @description Permite obtener valores del metodo GET (url)
	 */
	function getQueryVariable(variable) {
	   var query = window.location.search.substring(1);
	   var vars = query.split("&");
	   for (var i=0; i < vars.length; i++) {
	       var pair = vars[i].split("=");
	       if(pair[0] == variable) {
	           return pair[1];
	       }
	   }
	   return false;
	}


	// Comprobamos que la URL tenga esta variable
	if( getQueryVariable('register') == "success" ){

		// Crear Input
		const ALERTA = document.createElement("div"),
			    TEXTO = document.createTextNode("✅ ¡Ya te encuentras registrado!"),
			    VERIFICATION_BODY = document.getElementById('verification_body');

		ALERTA.appendChild(TEXTO); // add text
		ALERTA.classList.add('alert','alert-info'); // add class
		ALERTA.setAttribute("role", "alert"); // add role

		VERIFICATION_BODY.prepend(ALERTA); // add to html
	}

	/**
	 * @function validarCampo
	 * @param {Object} input - Proviene del evento keyup generado en la funcion "INPUT_FUNCTIONS"
	 * @description Permite verificar si se cumple la expresion regular /[\w]{1}/ y 
	 agrega una clase para modificar los bordes del input
	 */
	const validarCampo = input => {

		if( REGEXP.test(input.value) ){
			input.classList.add('valid_verification'); // green border
			input.classList.remove('verification_error');
			return true;
		}else{
			input.classList.add('verification_error'); // red border
			input.classList.remove('valid_verification');
			return false;
		}

	}


	/**
	 * @function INPUT_FUNCTIONS
	 * @description Recorre cada input de la constante CAMPOS y les asigna un evento keyup 
	 para comprobar si puede pasar al siguiente input
	 */
	const INPUT_FUNCTIONS = () => {

		for(let numeroDeCampo=0; numeroDeCampo < CANTIDAD_DE_CAMPOS; numeroDeCampo++ ){
		
			// Recorre desde el primer input hasta el penúltimo
			if( numeroDeCampo < CANTIDAD_DE_CAMPOS - 1 ){
				CAMPOS[numeroDeCampo].addEventListener('keyup', () => { 
					
					if( validarCampo(CAMPOS[numeroDeCampo]) ){
						CAMPOS[numeroDeCampo + 1].focus();
					}else{
						CAMPOS[numeroDeCampo].value = ''; // Limpiamos el campo
						CAMPOS[numeroDeCampo].focus();
					}
					 
				});
			}
			// Ultimo input de la lista
			else if( numeroDeCampo === CANTIDAD_DE_CAMPOS - 1 ){

				CAMPOS[numeroDeCampo].addEventListener('keyup', () => {

					if( validarCampo(CAMPOS[numeroDeCampo]) ){
						alert('Tu código esta siendo verificado.');
					}else{
						CAMPOS[numeroDeCampo].value = ''; // Limpiamos el campo
						CAMPOS[numeroDeCampo].focus();
					}
					
				});
			}

		}

	}


/* ------------ Eventos ----------- */
INPUT_FUNCTIONS();